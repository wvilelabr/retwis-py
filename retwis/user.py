from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

from retwis.auth import login_required
from retwis.db import get_db
from retwis.redis.user import User
from retwis.redis.post import Post

bp = Blueprint('user', __name__)


@bp.route('/')
def index():
    if not g.user:
        return render_template('home_not_logged.html')

    counts = g.user.followees_count, g.user.followers_count, g.user.tweet_count
    if len(g.user.posts()) > 0:
        last_tweet = g.user.posts()[0]
    else:
        last_tweet = None
    return render_template('timeline.html', timeline=g.user.timeline(), username=g.user.username,
                           counts=counts, last_tweet=last_tweet)


@bp.route('/<name>', methods=('GET',))
def user_page(name):
    is_following = False
    db = get_db()
    user = User.find_by_username(name, r=db)
    if user:
        counts = user.followees_count, user.followers_count, user.tweet_count
        logged_user = g.user
        himself = logged_user.username == name
        if logged_user:
            is_following = logged_user.following(user)

        return render_template('user.html', posts=user.posts(), counts=counts, page='user',
                               username=user.username, is_following=is_following, himself=himself)
    else:
        abort(404, f"User '{name}' not found")


@bp.route('/<name>/statuses/<int:post_id>')
def status(name, post_id):
    db = get_db()
    post = Post.find_by_id(post_id, r=db)
    logged_in = True if g.user else False
    if post:
        if post.user.username == name:
            return render_template('single.html', username=post.user.username, tweet=post, page='single',
                                   logged=logged_in)
    abort(404, 'Tweet not found')


@bp.route('/follow/<name>', methods=('POST',))
@login_required
def follow(name):
    db = get_db()
    user_to_follow = User.find_by_username(name, r=db)
    if user_to_follow:
        g.user.follow(user_to_follow)

    redirect('/%s' % name)


@bp.route('/unfollow/<name>', methods=('POST',))
@login_required
def unfollow(name):
    db = get_db()
    user_to_unfollow = User.find_by_username(name, r=db)
    if user_to_unfollow:
        g.user.stop_following(user_to_unfollow)

    redirect('/%s' % name)


@bp.route('/mentions')
def mentions():
    counts = g.user.followees_count, g.user.followers_count, g.user.tweet_count
    logged_in = True if g.user else False
    return render_template('mentions.html', mentions=g.user.mentions(), username=g.user.username,
                           counts=counts, posts=g.user.posts()[:1], logged=logged_in)
