from retwis.redis.post import Post


class Timeline:
    def __init__(self, r):
        self.r = r

    def page(self, page):
        _from = (page-1)*10
        _to = page*10
        return [Post(post_id, r=self.r) for post_id in self.r.lrange('timeline', _from, _to)]
