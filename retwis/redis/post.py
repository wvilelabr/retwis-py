import re

from retwis.redis.base import Model
from retwis import redis


class Post(Model):
    def __init__(self, *args, **kwargs):
        super(Post, self).__init__(*args, **kwargs)

    @staticmethod
    def create(user, content, r):
        post_id = r.incr("post:uid")
        post = Post(post_id, r)
        post.content = content
        post.user_id = user.id
        # post.created_at = Time.now.to_s
        user.add_post(post, r)
        r.lpush("timeline", post_id)
        for follower in user.followers:
            follower.add_timeline_post(post)

        mentions = re.findall(r'@\w+', content)
        for mention in mentions:
            u = redis.User.find_by_username(mention[1:], r)
            if u:
                u.add_mention(post, r)

    @staticmethod
    def find_by_id(post_id, r):
        pass

    @property
    def user(self):
        pass
