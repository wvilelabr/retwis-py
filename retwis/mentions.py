from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

from retwis.auth import login_required
from retwis.db import get_db

bp = Blueprint('mentions', __name__)


@bp.route('/')
@login_required
def index():
    counts = g.user.followees_count, g.user.followers_count, g.user.tweet_count
    return render_template('mentions.html', mentions=g.user.mentions(), username=g.user.username,
                           counts=counts, posts=g.user.posts()[:1])
