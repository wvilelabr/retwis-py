from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

from retwis.auth import login_required
from retwis.db import get_db

from retwis.redis import Post

bp = Blueprint('post', __name__)


@bp.route('/post', methods=('POST',))
@login_required
def post():
    content = request.form['content']
    db = get_db()
    Post.create(g.user, content, db)
    return redirect(url_for('user.index'))
